module.exports = {
    root: true,
    env: {
        node: true,
    },
    plugins: [
        "@typescript-eslint", 
        "smells",
        "filenames", 
        "eslint-plugin-local-rules"
    ],
    extends: [
        "plugin:vue/base",
        "plugin:vue/essential",
        "plugin:vue/strongly-recommended",
        "plugin:vue/recommended",

        "@vue/prettier",
        "@vue/typescript",
    ],
    rules: {
        "vue/no-v-html": "off",
        "vue/require-prop-types": "off",
        "vue/require-default-prop": "off",
        "vue/this-in-template": "error",
        "local-rules/no-data-name": "error",
        "local-rules/no-more-than-one-style-tag": "error",
        "vue/no-restricted-syntax": "error",
        "vue/v-on-function-call": "error",
        "@typescript-eslint/no-unused-vars": "error",
        "filenames/match-regex": [2, "^[a-z0-9_.-]+$", true],
        "no-console": "error",
        "no-debugger": "error",
        "no-alert": "error",
        "no-var": "error",
        "smells/no-switch": "error",
        "smells/no-complex-switch-case": "error",
        "smells/no-setinterval": "error",
        "smells/no-this-assign": "error",
        "no-unexpected-multiline": "error",

        // это tslint правила я (Петр Лаптев) почему-то здесь не смог из подключить

        //"member-access": [true, "no-public"],
        // "variable-name": [
        //     true,
        //     "ban-keywords",
        //     "check-format",
        //     "allow-pascal-case",
        //     "allow-leading-underscore",
        //     "allow-snake-case",
        // ],
        // quotemark: [true, "double"],
        // "ordered-imports": false,
        // "import-spacing": true,
        // "object-literal-sort-keys": false,
        // "no-consecutive-blank-lines": false,
        // "no-trailing-whitespace": true,
        // semicolon: [true, "always"],
        // eofline: true,
        // "trailing-comma": [
        //     true,
        //     {
        //         multiline: "always",
        //         singleline: "never",
        //     },
        // ],
        // "interface-name": false,
        // whitespace: [
        //     true,
        //     "check-branch",
        //     "check-decl",
        //     "check-operator",
        //     "check-module",
        //     "check-separator",
        //     "check-rest-spread",
        //     "check-type",
        //     "check-type-operator",
        // ],
    },
    parserOptions: {
        parser: "@typescript-eslint/parser",
    },
};
