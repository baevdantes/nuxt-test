import ICategory from "~/interfaces/ICategory"

export default interface IAppState {
  categories: ICategory[]
}
