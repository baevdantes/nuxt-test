export default interface ICategory {
  id: number
  name: string
  price_multiplier: number
  game_id: number
  created_at: number
  updated_at: number
  sort: number
  commission: number
  show_price: number
  show_count: number
  show_time: number
  multiple_sale: number
  status: number
  parent_id: number
  is_instant_delivery: number
  is_allow_multiple_offers: boolean
  isFinal: bigint
  children: ICategory[]
}
